import { h, Component } from 'preact';
import Card from 'preact-material-components/Card';
import Slider from 'preact-material-components/Slider';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';
import 'preact-material-components/Card/style.css';
import 'preact-material-components/Button/style.css';
import 'preact-material-components/Slider/style.css';
import style from './style';

export default class Home extends Component {
	state = {
		sliderValue: 1
	}
	handleSliderChange = (e) => {
		e.preventDefault();
		if (typeof e.detail.value === 'number') {
			this.setState({
				sliderValue: e.detail.value
			});
		}
	}
	mappedValue = (x) => Math.round(Math.log10(x) * 2000, 0)
	render() {
		const { sliderValue } = this.state;
		return (
			<div class={`${style.home} page`}>
				<h1>Volume Control</h1>
				<Card>
					<div class={style.cardHeader}>
						<div class=" mdc-typography--caption">Change slide to get value</div>
					</div>
					<div class={style.cardBody}>
						<Slider min={0} max={1} value={sliderValue} onChange={this.handleSliderChange} />
						<TextField label="mapped value" value={this.mappedValue(sliderValue)} />
					</div>
				</Card>
			</div>
		);
	}
}
