# What's this

Map raspberry pi media player application omxplayer's volume

[![Netlify Status](https://api.netlify.com/api/v1/badges/20baf177-31f4-49ca-a542-0c6b5e4576bc/deploy-status)](https://app.netlify.com/sites/omxplayer-volume-mapping/deploys)

# preact material app

Super performant `Material` app for preact world using [preact-material-components](https://github.com/prateekbh/preact-material-components)

## CLI Commands

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# test the production build locally
npm run serve
```

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).
